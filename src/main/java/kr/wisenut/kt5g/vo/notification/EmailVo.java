package kr.wisenut.kt5g.vo.notification;

import java.util.Objects;

public class EmailVo {
    String subject;
    String body;
    // 승인된 발신자 Email 주소
    String sender;

    String sender_name;


    public EmailVo() {

    }

    public EmailVo(String subject, String body, String sender, String sender_name) {
        this.subject = subject;
        this.body = body;
        this.sender = sender;
        this.sender_name = sender_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmailVo emailVo = (EmailVo) o;
        return Objects.equals(subject, emailVo.subject) &&
                Objects.equals(body, emailVo.body) &&
                Objects.equals(sender, emailVo.sender) &&
                Objects.equals(sender_name, emailVo.sender_name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(subject, body, sender, sender_name);
    }

    @Override
    public String toString() {
        return "EmailVo{" +
                "subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", sender='" + sender + '\'' +
                ", sender_name='" + sender_name + '\'' +
                '}';
    }
}


