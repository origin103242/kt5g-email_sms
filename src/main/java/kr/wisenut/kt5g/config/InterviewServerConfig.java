package kr.wisenut.kt5g.config;

import org.aeonbits.owner.Config;

@Config.Sources({"file:${user.dir}/config/server.properties",
        "file:${user.dir}/../config/server.properties"})
public interface InterviewServerConfig extends Config {
    @Key("interview.server.ip")
    @DefaultValue("61.82.137.170")
    String getInterviewServerIp();

    @Key("interview.server.port")
    @DefaultValue("18001")
    String getInterviewServerPort();
}
